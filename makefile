#Infos: http://www.ijon.de/comp/tutorials/makefile.html


VERSION = 1.0
CC      = cc
CFLAGS  = -Wall -O3 -D_REENTRANT -DVERSION=\"$(VERSION)\"
#LDFLAGS = -lm -lpthread `gtk-config --cflags` `gtk-config --libs` -lgthread
LDFLAGS = -lncurses

OBJ = hangman.o prng.o

all: $(OBJ)
	$(CC) $(CFLAGS) -o hangman $(OBJ) $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -r *.o
