#include <time.h>


/* Defined macros */
#define DEFAULTTRIES 6
#define MAXWORDLENGTH 200
#define ALPHABET 26
#define ALPHABET_NUM 36
#define ANIM_DURATION 1
#define LINEBREAK 16 //Linebreak offset
#define HITFEEDSLOTS 5
#define IMPRESSIVESTREAK 3 //reach amount of hit streaks to activate impressive screen
#define IMPRESSIVEHIT 2

/* Data structures */
typedef struct {
    int moves; //Only wrong moves
    int allowedMoves;
    int guesses; //All guesses included valid moves
    int maxy;
    int maxx;
    int centery;
    int centerx;
    char guessWord[MAXWORDLENGTH];
    int wordLength;
    char currentWord[MAXWORDLENGTH];
    char wrongCharacters[DEFAULTTRIES];
    int trollEnabled;
    char alphabet[ALPHABET_NUM + 1];
    time_t startTime;
    time_t endTime;
    int wordRows;
} game_state;

typedef struct word {
    char character;
    struct word *next;
} word;

typedef struct hitfeed {
    char history[HITFEEDSLOTS][50]; //Prints the last 3 streaks
    char beststreak[50]; //Prints the best streak
    int besthit; //Prints the best hit number e.g. Godlike with 15 hits
    int impstreakcounter; //if this counter hits IMPRESSIVESTREAK there'll be an animation
} hitfeed;

/* Function prototypes */
void quitProgram(void);
void updateScreen(game_state *gs);
void initCoordinates(game_state *gs);
void initGuessWord(game_state *gs, char *filename);
void initAlphabet(game_state *gs);
void drawAlphabet(game_state *gs, char usedchar);
void drawGuessWord(game_state *gs);
int playerInput(game_state *gs, hitfeed *hf);
int fillCurrentWord(game_state *gs, char validchar);
int stackWrongCharacter(game_state *gs, char wrongchar);
int checkWin(game_state *gs);
void printGameStats(game_state *gs);
void toLowerCase(char *str);
void showStartScreen(game_state *gs);
void startGame(game_state *gs, hitfeed *hf);
void showHelp(game_state *gs);
int centerDiff(int coordinate, char *str);
int readRandomLine(char *file, char *result);
void trollHitScreen(game_state *gs, hitfeed *hf, int hits);
void addHitToFeed(hitfeed *hf, char *streak, int hit);
void trollHandleImpressive(hitfeed *hf, game_state *gs, int hits);
void drawFigure(game_state *gs, int drawNext);
void animateLineClear(game_state *gs, int line, int offsetMultiplier);
void printHitFeed(game_state *gs, hitfeed *hf);
